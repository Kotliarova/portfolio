const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

gulp.task('bundleSass', function () {
    return gulp.src('src/sass/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('watch', function () {
    gulp.watch('src/sass/**/*.scss', gulp.series('bundleSass'));
});
