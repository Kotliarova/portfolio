const arrContent = [
    {graphic: 'img/works/graphic-design/graphic-design1.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design2.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design3.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design4.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design5.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design6.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design7.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design8.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design9.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design10.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design11.jpg'},
    {graphic: 'img/works/graphic-design/graphic-design12.jpg'},
    {web: 'img/works/web-design/web-design1.jpg'},
    {web: 'img/works/web-design/web-design2.jpg'},
    {web: 'img/works/web-design/web-design3.jpg'},
    {web: 'img/works/web-design/web-design4.jpg'},
    {web: 'img/works/web-design/web-design5.jpg'},
    {web: 'img/works/web-design/web-design6.jpg'},
    {web: 'img/works/web-design/web-design7.jpg'},
    {landing: 'img/works/landing-page/landing-page1.jpg'},
    {landing: 'img/works/landing-page/landing-page2.jpg'},
    {landing: 'img/works/landing-page/landing-page3.jpg'},
    {landing: 'img/works/landing-page/landing-page4.jpg'},
    {landing: 'img/works/landing-page/landing-page5.jpg'},
    {landing: 'img/works/landing-page/landing-page6.jpg'},
    {landing: 'img/works/landing-page/landing-page7.jpg'},
    {wordpress: 'img/works/wordpress/wordpress1.jpg'},
    {wordpress: 'img/works/wordpress/wordpress2.jpg'},
    {wordpress: 'img/works/wordpress/wordpress3.jpg'},
    {wordpress: 'img/works/wordpress/wordpress4.jpg'},
    {wordpress: 'img/works/wordpress/wordpress5.jpg'},
    {wordpress: 'img/works/wordpress/wordpress6.jpg'},
    {wordpress: 'img/works/wordpress/wordpress7.jpg'},
    {wordpress: 'img/works/wordpress/wordpress8.jpg'},
    {wordpress: 'img/works/wordpress/wordpress9.jpg'},
    {wordpress: 'img/works/wordpress/wordpress10.jpg'}
]

const tabs = document.querySelector('.servis .tabs');
const tabsContent = document.querySelectorAll('.servis-tabs-contents');
const btnLoad = document.querySelector('.works .btn');
const content = document.querySelectorAll('.works-content .content');
const tabsWorks = document.querySelector('.works .tabs');


tabs.addEventListener('click', ev => {
    const prevActiveTab = document.querySelector('.tab.active');
    prevActiveTab.classList.remove('active');
    ev.target.classList.add('active');
    const prevActiveTabContent = document.querySelector('.servis-tabs-content.active');
    prevActiveTabContent.classList.remove('active');
    const activeTab = ev.target.getAttribute('data-name');
    const activeTabContent = document.querySelector(`.servis-tabs-content[data-name=${activeTab}]`);
    activeTabContent.classList.add('active');
})

function createImg(type) {
    let div = document.createElement('div');
    div.classList.add('content');
    let img = document.createElement('img');
    img.classList.add('img');
    img.setAttribute('src', type);
    div.appendChild(img);

    let divContentHover = document.createElement('div');
    divContentHover.classList.add('content-hover');

    let divContentFunction = document.createElement('div');
    divContentFunction.classList.add('content-function');
    let divCircle = document.createElement('div');
    divCircle.classList.add('circle');
    let divCircleImg = document.createElement('div');
    divCircleImg.classList.add('circle');
    let imgCircle = document.createElement('img');
    imgCircle.setAttribute('src', `../img/link.svg`);
    divCircleImg.appendChild(imgCircle);
    divContentFunction.appendChild(divCircleImg);
    divContentFunction.appendChild(divCircle);

    let divContentText = document.createElement('div');
    divContentText.classList.add('content-text');
    let spanContentText = document.createElement('span');
    spanContentText.textContent = 'creative design';
    divContentText.textContent = 'Web Design';
    divContentText.prepend(spanContentText);

    divContentHover.appendChild(divContentFunction);
    divContentHover.appendChild(divContentText);

    div.appendChild(divContentHover);

    document.querySelector('.works-content').appendChild(div);
}

btnLoad.addEventListener('click', ev => {
    for (i = 1; i <= 12; i++) {
        createImg(`img/load-img/load-img${i}.jpg`);
    }
    btnLoad.addEventListener('click', ev => {
        for (i = 1; i <= 0; i++) {
            createImg(`img/load-img/load-img${i}.jpg`);
        }
        btnLoad.style.display = 'none';
    })

})

tabsWorks.addEventListener('click', ev => {
    const prevActiveTab = document.querySelector('.works-active');
    prevActiveTab.classList.remove('works-active');
    ev.target.classList.add('works-active');
    let typeImg = ev.target.getAttribute('data-name');
    const worksContant = document.querySelectorAll('.content');
    worksContant.forEach(elem => {
        return elem.classList.add('delete');
    });
    arrContent.filter(content => {
        if(typeImg === 'graphic' && content.graphic) {
            return createImg(`${content.graphic}`)
        } else if (typeImg === 'web' && content.web) {
            return createImg(`${content.web}`)
        } else if (typeImg === 'landing' && content.landing) {
            return createImg(`${content.landing}`)
        } else if (typeImg === 'wordpress' && content.wordpress) {
            return createImg(`${content.wordpress}`)
        } else if (typeImg === 'all') {
            worksContant.forEach(elem => {
                return elem.classList.remove('delete');
            });
        }
    });
})

$(document).ready(function(){
    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider',
        centerMode: false,
        focusOnSelect: true,
        variableWidth: true
    });

});
